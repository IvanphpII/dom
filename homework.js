//Задание номер 1
var table = document.getElementsByTagName('td');
for (var i = 0; i < table.length; i++) {
	if ((table[i].firstChild.nodeValue === '2.') || (table[i].firstChild.nodeValue === '4.') || (table[i].firstChild.nodeValue === '6.')) {
		table[i].style.color = '#FFFFFF';
		table[i].style.backgroundColor = '#333333';
	}
}

//Задание номер 2
var divFirst = document.getElementsByClassName('bar first');
var a = divFirst[0].innerHTML;
var divLast = document.getElementsByClassName('bar last');
var b = divLast[0].innerHTML;
divFirst[0].innerHTML = b;
divLast[0].innerHTML = a;

//Задание номер 3
var removeItem = document.querySelectorAll('.removeSecondBlockHere > div');
	for (var i = 0; i < removeItem.length; i++) {
		if (removeItem[i].innerHTML.trim() == '2. Текст блока №2') {
 			removeItem[i].parentNode.removeChild(removeItem[i]);
		}
	}

